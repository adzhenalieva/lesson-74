const express = require('express');
const fs = require('fs');
const path = "./messages";
let files = [];

let filename = 'messages/' + JSON.stringify(new Date()) + '.txt';
const router = express.Router();

router.get('/', (req, res) => {
    fs.readdir(path, (err, files) => {
        let fileText = [];
        files = files.slice(-5);
        files.forEach(file => {
            const fileContent = fs.readFileSync(path + '/' + file);
            fileText.push(JSON.parse(fileContent));
        });
        res.send(fileText);
    });
});


router.post('/', (req, res) => {
    files.push(filename);
    fs.writeFileSync(filename, JSON.stringify(req.body));
    res.send({message: req.body.message, datetime: filename});
});

module.exports = router;